const ContactModel = require('../models/contact.js')

const Contact = class Contact {
  /**
   * @constructor
   * @param {Object} app
   * @param {Object} config
   */
  constructor (app, connect) {
    this.app = app
    this.ContactModel = connect.model('Contact', ContactModel)

    this.run()
  }

  create () {
    this.app.post('/contact/', (req, res) => {
      try {
        const contactModel = new this.ContactModel(req.body);

        contactModel.save().then((contact) => {
          res.status(200).json(contact || {})
        }).catch(() => {
          res.status(403).json({
            code: 403,
            message: 'Bad request'
          })
        })
      } catch (err) {
        console.error(`[ERROR] POST contacts/ -> ${err}`)

        res.status(500).json({
          code: 500,
          message: 'Internal server error'
        })
      }
    })
  }

    all () {
      this.app.get('/contacts/', (req, res) => {
        try {
          this.ContactModel.find().sort({ createdAt: -1 }).then((contact) => {
            res.status(200).json(contact || {})
          }).catch(() => {
            res.status(403).json({
              code: 403,
              message: 'Bad request'
            })
          })
        } catch (err) {
          console.error(`[ERROR] POST contacts/ -> ${err}`)
  
          res.status(500).json({
            code: 500,
            message: 'Internal server error'
          })
        }
      })
    }

    delete () {
      this.app.delete('/contact/:id', (req, res) => {
        try {
          const token = req.headers.authorization.split(' ')[1]
          if (!token) {
            return res.status(401).json({
              code: 401,
              message: 'Unauthorized'
            })
          }

          this.ContactModel.findByIdAndDelete(req.params.id).then((contact) => {
            res.status(200).json(contact || {})
          }).catch(() => {
            res.status(403).json({
              code: 403,
              message: 'Bad request'
            })
          })
        } catch (err) {
          console.error(`[ERROR] POST contacts/ -> ${err}`)
  
          res.status(500).json({
            code: 500,
            message: 'Internal server error'
          })
        }
      })
    }

  /**
   * Run
   */
  run () {
    this.delete()
    this.all()
    this.create()
  }
}

module.exports = Contact
